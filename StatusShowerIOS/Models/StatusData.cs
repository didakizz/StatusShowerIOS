﻿using System;

namespace StatusShower
{
    public class StatusData
    {
        public StatusData(string name, string message, string bundleResourceId)
        {
            this.Name = name;
            this.Message = message;
            this.BundleResourceId = bundleResourceId;
        }

        public string Name { get; private set; }
        public string BundleResourceId { get; private set; }
        public string Message { get; private set; }
    }
}
