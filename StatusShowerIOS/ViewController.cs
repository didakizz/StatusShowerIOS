﻿using System;

using UIKit;
using StatusShowerIOS.UI;
using StatusShower;
using System.Collections.Generic;

namespace StatusShowerIOS
{
    public partial class ViewController : UIViewController
    {
        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Create and set up table
            var table = new UITableView(View.Bounds); 

            // Get status data mock items
            var tableItems = new List<StatusData> {
                new StatusData("Pesho","How ya doing?", "randompic0.jpg"),
                new StatusData("Gosho","Cant complain, right?", "randompic1.jpg"),
                new StatusData("Tosho","Not really", "randompic2.jpg"),
                new StatusData("Angel","Wow, maaaaaan, life is good", "randompic3.jpg")
            };

            // Set table's source
            table.Source = new StatusesTableSource(tableItems);

            // Add table to view
            Add(table);
        }
    }
}
