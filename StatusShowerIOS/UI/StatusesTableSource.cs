﻿using System;
using Foundation;
using UIKit;
using System.Collections.Generic;
using StatusShower;

namespace StatusShowerIOS.UI
{
    public class StatusesTableSource : UITableViewSource
    {
        // Table's variables
        float cellHeight = 140;

        List<StatusData> TableItems;
        string CellIdentifier = "StatusCell";

        // Table's functions
        public StatusesTableSource(List<StatusData> items)
        {
            TableItems = items;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            StatusDataTableViewCell cell = tableView.DequeueReusableCell(CellIdentifier) as StatusDataTableViewCell;
            StatusData item = TableItems[indexPath.Row];

            // If there are no cells to reuse, create a new one
            if (cell == null)
            {
                cell = new StatusDataTableViewCell(CellIdentifier);
            }

            cell.UpdateCell(TableItems[indexPath.Row]);

            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return TableItems.Count;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return cellHeight;
        }
    }
}
