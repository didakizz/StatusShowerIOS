﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;
using StatusShower;

namespace StatusShowerIOS.UI
{
    public class StatusDataTableViewCell: UITableViewCell
    {
        // Cell's UI properties
        UILabel messageLabel, nameLabel;
        UIImageView imageView;

        public StatusDataTableViewCell(string cellId): base(UITableViewCellStyle.Default, cellId)
        {
            SelectionStyle = UITableViewCellSelectionStyle.None;

            imageView = new UIImageView();
            messageLabel = new UILabel();
            nameLabel = new UILabel();

            ContentView.AddSubviews(new UIView { messageLabel, imageView, nameLabel });
        }

        // Update cell with valid given status data
        public void UpdateCell(StatusData statusData)
        {
            if (statusData != null)
            {
                imageView.Image = UIImage.FromBundle(statusData.BundleResourceId);
                messageLabel.Text = statusData.Message;
                nameLabel.Text = statusData.Name;
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            // Local vars
            var labelHeight = 20;
            var imageViewHeight = 100;
            var shadowRadius = 5;
            var shadowOpacity = 1.0f;

            //  Update Labels layout
            messageLabel.Frame = new CGRect(0, 0, ContentView.Bounds.Width, labelHeight);
            messageLabel.TextAlignment = UITextAlignment.Center;

            // Add shadow to message label
            messageLabel.Layer.ShadowRadius = shadowRadius;
            messageLabel.Layer.ShadowColor = UIColor.Black.CGColor;
            messageLabel.Layer.ShadowOffset = new CGSize(shadowRadius, shadowRadius);
            messageLabel.Layer.ShadowOpacity = shadowOpacity;

            nameLabel.Frame = new CGRect(0, ContentView.Bounds.Height - labelHeight, ContentView.Bounds.Width, labelHeight);
            nameLabel.TextAlignment = UITextAlignment.Center;

            // Update ImageView layout
            imageView.Frame = new CGRect(ContentView.Center.X - (imageViewHeight / 2), labelHeight, imageViewHeight, imageViewHeight);
            imageView.Layer.CornerRadius = imageView.Bounds.Height / 2;
            imageView.Layer.MasksToBounds = true;
        }
    }
}
